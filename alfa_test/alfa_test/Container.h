#pragma once

#include <vector>
#include <map>
#include <stdexcept>
#include <shared_mutex>

#include <iostream>

template<typename KeyType, typename ValueType>
struct Element
{
	KeyType k;
	ValueType v;
};

template<typename KeyType, typename ValueType>
class Container
{
public:

	ValueType at(const KeyType& key)
	{
		std::shared_lock locker(mtx);
		auto it = std::find_if(elements.begin(), elements.end(), [&key](const auto& kv) {
			if (kv.k == key)
				return true;
			return false;
			});

		if (it == elements.end())
			throw std::out_of_range("Cant find element for key");

		return it->v;
	}

	bool insert(const Element<KeyType, ValueType>& element)
	{
		std::unique_lock locker(mtx);
		auto it = std::find_if(elements.begin(), elements.end(), [&key = element.k](const auto& kv) {
			if (kv.k == key)
				return true;
			return false;
		});

		if (it != elements.end())
			return false;

		elements.push_back(element);

		return true;
	}

	size_t size()
	{
		std::shared_lock locker(mtx);
		return elements.size();
	}

	void clear()
	{
		std::unique_lock locker(mtx);
		elements.clear();
	}

	size_t erase(const KeyType& key)
	{
		std::unique_lock locker(mtx);
		auto it = std::find_if(elements.begin(), elements.end(), [&key](const auto& kv) {
			if (kv.k == key)
				return true;
			return false;
		});

		if (it == elements.end())
			return 0;

		elements.erase(it);
		return 1;
	}

private:
	std::vector<Element<KeyType, ValueType>> elements;
	std::shared_mutex mtx;
};

