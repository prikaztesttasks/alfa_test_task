﻿#include <iostream>
#include <cassert>
#include <thread>
#include <array>

#include "Container.h"

/// <summary>
/// Tools
/// </summary>
class TestKeyType
{
public:
    TestKeyType(int x_, char c_)
        : x(x_), c(c_)
    {}

    bool operator==(const TestKeyType& other) const 
    {
        if (x == other.x && c == other.c)
            return true;
        return false;
    }

private:
    int x;
    char c;
};


/// <summary>
/// Тесты
/// </summary>
void test1()
{
    Container<int, int> c;
    c.insert({ 1,2 });
    c.insert({ 1,2 });
    c.insert({ 1,2 });
    c.insert({ 1,2 });

    assert(!c.insert({ 1,2 }));
    assert(c.at(1) == 2);
    assert(c.size() == 1);
}

void test2()
{
    Container<int, std::string> c;
    c.insert({ 1, "val1" });
    c.insert({ 2, "val2" });

    bool isException = false;
    try
    {
        c.at(3);
    }
    catch (const std::exception& e)
    {
        isException = true;
    }

    assert(isException);
    assert(c.at(1) == "val1");
    assert(c.at(2) == "val2");
    assert(c.size() == 2);
}

void test3()
{
    Container<std::string, char> c;
    c.insert({ "val1", 'a' });
    c.insert({ "val2", 'b' });
    c.insert({ "val3", 'c' });

    assert(c.at("val1") == 'a');
    assert(c.at("val3") == 'c');
    assert(c.size() == 3);
}

void test4()
{
    Container<char, float> c;
    c.insert({ 'a', 0.1f });
    c.insert({ 'b', 100.1f });
    c.insert({ 'c', 999.3f });

    assert(c.size() == 3);

    c.erase('b');

    assert(c.size() == 2);

    c.clear();

    assert(c.size() == 0);
}

void test5()
{
    Container<TestKeyType, int> c;
    c.insert({ TestKeyType(0, 'v'), 'a'});
    c.insert({ TestKeyType(2, 'x'), 'b'});
    
    assert(c.size() == 2);
    assert(c.at(TestKeyType(0, 'v')) == 'a');
    assert(c.at(TestKeyType(2, 'x')) == 'b');
}

void test6()
{
    Container<int, int> c;

    std::thread writer = std::thread([&c]()
        {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            for (int i = 0; i < 1000; ++i)
                c.insert({ i, i * 2 });
            std::this_thread::sleep_for(std::chrono::seconds(1));
            for (int i = 1000; i < 2000; ++i)
                c.insert({ i, i * 2 });
        });

    std::array<std::thread, 10> readers;
    for (auto& el : readers)
    {
        el = std::thread([&c]()
            {
                while (c.size() != 2000)
                    std::this_thread::sleep_for(std::chrono::milliseconds(100));
            });
    }

    if(writer.joinable())
        writer.join();

    for (auto& el : readers)
        if (el.joinable())
            el.join();

    assert(c.size() == 2000);
}

void test7()
{
    Container<int, int> c;

    std::array<std::thread, 10> writers;

    for (int j = 0; j < 10; ++j)
    {
        writers[j] = std::thread([&c, j]()
            {
                std::this_thread::sleep_for(std::chrono::seconds(1));
                for (int i = 1; i <= 1000; ++i)
                    c.insert({ i + (j * 10000) , i * 2 });
                std::this_thread::sleep_for(std::chrono::seconds(1));
                for (int i = 1001; i <= 2000; ++i)
                    c.insert({ i + (j * 20000), i * 2 });
            });
    }

    std::array<std::thread, 10> readers;
    for (auto& el : readers)
    {
        el = std::thread([&c]()
            {
                while (c.size() != 20000)
                    std::this_thread::sleep_for(std::chrono::milliseconds(100));
            });
    }

    for (auto& el : writers)
        if (el.joinable())
            el.join();

    for (auto& el : readers)
        if (el.joinable())
            el.join();

    assert(c.at(2000 +(9 * 20000)) == 2000 * 2);
    assert(c.size() == 20000);
}

int main()
{
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();

    std::cout << "Done!" << std::endl;

    return 0;
}